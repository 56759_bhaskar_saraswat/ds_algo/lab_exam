package lab_exam;

public class stack {

public class Stack {
	class Node{
		private books book;
		private Node next;
		
		public Node(books book) {
			this.book = book;
		}
		
	}
	private Node head;
  private Node top;
  private int count;
  
  public Stack() {
	  
  }
  public boolean isEmpty() {
	  if(head==null) {
		  return true;
	  }
	  
	  return false;
  }
  public boolean isFull() {
	  if(count==5) {
		  return true;
	  }
	  
	  return false;
  }
  
  
  public void push(books book) {
	  Node newNode = new Node(book);
	  if(isEmpty()) {
		  head = newNode;
		  top = newNode;
		  count++;
	  }else if(isFull()){
		  System.out.println("stack overflow");
		 
	  }else {
		  Node temp = top;
		  top.next = newNode;
		  top = newNode; 
		  top.next = head;
		  count++;
	  }
  }
  
  public void pop() {
	  if(isEmpty()) {
		  System.out.println("stack underflow");
	  }
	  
	  Node temp = head;
	  while(temp.next != top) {
		  temp = temp.next;
	  }
	  top = temp;
	  top.next = head;
  }
  
  public books peek() {
	  return top.book;
  }
  
//  public void display() {
//	  Node temp = head;
//	  while(temp != null) {
//		  System.out.println(temp.data);
//		  temp = temp.next;
//	  }
//  }
  

}

}
