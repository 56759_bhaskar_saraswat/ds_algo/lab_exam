package lab_exam;

public class StackUsingDLLL {
	   
//	   static class Node {
//	       private Node prev;
//	       private books data;
//	       private Node next;
//	       
//	       public Node(books temp) {
//	           this.prev = null;
//	           this.data = temp;
//	           this.next = null;
//	       }
	//   }
	   
	   private Node top;
	   private int nodesCount;
	   
	   public StackUsingDLLL() {
	       this.top = null;
	       this.nodesCount = 0;
	   }
	   
	   public boolean Empty() {
	       return top == null;
	   }
	   
	   public void display() {
	       
	       if(Empty()) {
	           System.out.println("Stack is Empty !!!");
	       }
	       else {
	           
	           Node trav = top;
	           
	           System.out.println("Nodes Count : " + nodesCount);
	           System.out.print("Stack Elements Are : ");
	           
	           System.out.print("top -> ");
	           while(trav != null) {
	               System.out.print(trav.data + " -> ");
	               trav = trav.next;
	           }
	           System.out.println("null");
	       }
	       System.out.println();
	   }
	   
	   public void push(books temp) {
	       
	       Node newNode = new Node(temp);
	       
	       if(Empty()) {
	           top = newNode;
	           nodesCount++;
	       }
	       else {
	           newNode.next = top;
	           top.prev = newNode;
	           top = newNode;
	           nodesCount++;
	       }
	   }
	   
	   public void pop() {
	       
	       if(Empty()) {
	           System.out.println("Stack is Empty !!!");
	       }
	       else {
	           if(top.next == null) {
	               top = null;
	               nodesCount--;
	               System.out.println("Element Popped...");
	           }
	           else {
	               top = top.next;
	               top.prev = null;
	               nodesCount--;
	               System.out.println("Element Popped...");
	           }
	       }
	   }
	   
	   public int peek() {
	       
	       if(Empty()) {
	           System.out.println("Stack is Empty !!!");
	           return -1;
	       }
	       else {
	           return top.data;
	       }
	   }
	   
	 
	}


