package lab_exam;

import java.util.Scanner;
import java.util.Stack;

public class Test {
	
	public static  Scanner sc = new Scanner (System.in);
	public static int menuList() {
		System.out.println("0.Exit");
		System.out.println("1.Push");
		System.out.println("2.Pop");
		System.out.println("3.Top");
		return sc.nextInt();
	}
	
	public static books acceptRecord() {
		books book = new books();
		System.out.println("Enter book name");
		books.setName(sc.next());
		System.out.println("Enter author name");
		books.setAuthor(sc.next());
		System.out.println("Enter no. of pages");
		books.setPages(sc.next());
		System.out.println("Enter book price");
		books.setPrice(sc.next());
	}
	public static void main(String[] args) {
		int choice ;
		books temp = new books();
		StackUsingDLLL stk = new StackUsingDLLL();
		 while ((choice = Test.menuList())!=0) {
			 try {
				 switch (choice) {
				 case 1:
					 temp = Test.acceptRecord();
					 System.out.println(temp);
					 stk.push(temp);
					 break;
				 case 2 :
					 stk.pop();
					 break;
				 case 3:
					 stk.top();
					 break;
				 }

			 }
		 
			 
			 catch(InvalidExceptions e)
			 {System.out.println(e.getMessage());
			 }
		 }
		
		
	}
}

