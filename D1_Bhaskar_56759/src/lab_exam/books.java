package lab_exam;

public class books {
	private String name;
	private String author;
	private String pages;
	private int price;
	public books() {
		super();
		// TODO Auto-generated constructor stub
	}
	public books(String name, String author, String pages, int price) {
		super();
		this.name = name;
		this.author = author;
		this.pages = pages;
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPages() {
		return pages;
	}
	public void setPages(String pages) {
		this.pages = pages;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "books [name=" + name + ", author=" + author + ", pages=" + pages + ", price=" + price + "]";
	}

}
